from setuptools import setup, find_packages

setup(
    name='DAZZLER',
    description='Bad script that dumps threads into a text file.',
    packages=find_packages(),
    include_package_data=True, install_requires=['requests', 'beautifulsoup4', 'colorama']
)
