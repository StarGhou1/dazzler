import bs4
import requests
import urllib
from collections import OrderedDict
from thread_archiver.archived_thread import ArchivedThread


def get_reply_count(post_link):
    post_request = requests.get(post_link)
    post_content = post_request.content
    post_parsed = bs4.BeautifulSoup(post_content, features="html.parser")
    post_meta = post_parsed.find("div", "pull-right").contents[0]  # [replies / images / posters]
    post_replies = post_meta[1:].split(' ')[0]

    return int(post_replies)


def scrape_threads(subject, starting_page=1):
    thread_dict = {}  # thread number : thread object
    archived_threads = []  # where the final results go

    starting_num = int(starting_page)
    if starting_num <= 0:
        raise ValueError("Starting page can not be less than 1.")

    current_page = starting_num
    subject_encoded = urllib.parse.quote(subject)

    while current_page >= 1:
        seed = f"https://desuarchive.org/_/search/boards/mlp.desu.meta/subject/{subject_encoded}/page/{str(current_page)}/"
        seed_request = requests.get(seed)
        seed_content = seed_request.content
        seed_parsed = bs4.BeautifulSoup(seed_content, features="html.parser")
        post_articles = seed_parsed.find_all("article", "post")
        error = seed_parsed.find_all("div", "alert")

        if len(error) != 0 and "No results found." in error[0].text:
            return archived_threads

        for post in post_articles:
            post_title = post.find("h2", "post_title").contents[0]
            post_id = post.find_all("span", "post_controls")[0].find_all("a")[1].get("data-post-id")
            post_link = f"https://desuarchive.org/mlp/thread/{str(post_id)}"

            new_thread_object = ArchivedThread(post_title, post_link, post_id)

            if not str(new_thread_object.thread_number).startswith("RAND-") and new_thread_object.thread_number in thread_dict:
                stored_thread_object = thread_dict.get(new_thread_object.thread_number)
                stored_thread_replies = get_reply_count(stored_thread_object.thread_link)
                new_thread_replies = get_reply_count(new_thread_object.thread_link)
                if stored_thread_replies > new_thread_replies:
                    new_thread_object.thread_link = stored_thread_object.thread_link

            thread_dict[new_thread_object.thread_number] = new_thread_object
        current_page -= 1

    sorted_thread_dict = OrderedDict(sorted(thread_dict.items(), key=lambda t: t[0]))
    for x in sorted_thread_dict.items():
        archived_threads.append(str(x[1]))

    archived_threads.reverse()

    return archived_threads
