from random import randint


class ArchivedThread:
    def __init__(self, thread_title, thread_link, thread_id):
        self.thread_title = thread_title
        self.thread_link = thread_link
        self.thread_id = thread_id
        if '#' in self.thread_title:
            num = self.thread_title.split('#')[1].split(' ')[0]
            self.thread_number = int(num)
        else:
            self.thread_number = f"RAND-{randint(0, 999999)}"  # The reason I do this stupid shit instead of just
            #   setting it to None is because if every thread has an ID of None, it'll break the thread_dict
            #   in archive_populator. So this garbage will do for now.

    def __repr__(self):
        if str(self.thread_number).startswith("RAND-"):
            return repr(f"{self.thread_title} : {self.thread_link}")
        return repr(f"{self.thread_number}. {self.thread_link}")
