#!/usr/bin/env python3.7
from auxiliary.color_utils import *
from thread_archiver.archive_populator import scrape_threads


def print_brand():
    print(f"|--------------------------------------|\n"
          f"|               {Fore.YELLOW}DAZ{Fore.MAGENTA}ZL{Fore.CYAN}ER{Fore.GREEN}!{Style.RESET_ALL}               |\n"
          f"|--------------------------------------|\n")


if __name__ == "__main__":
    print_brand()

    # "well hey it works, right?"
    try:
        print_question("What subject do I search for?")
        subject = input("     ")
        print_question("And what page number should I start counting back from? (default: 1)")
        page_number = input("     ")
        print("ok :)")

        thread_list = scrape_threads(subject, page_number)
        if len(thread_list) <= 1:
            print_info("No threads were scraped. Does that page number have any results?")
            quit(0)

        print_general(f"Success - acquired {len(thread_list)} objects.")
        print_info("Dumping to file...")
        with open("threads.txt", 'w+') as output:
            for thread in thread_list:
                if thread.startswith("'") and thread.endswith("'"):
                    output.write(f"{thread[1:len(thread)-1]}\n")
                else:
                    output.write(f"{thread}\n")
        print_general("File dumped!")

    except Exception as ex:
        print_error(f"Error: {ex}")
